package net.mcreator.the_ultimate_combat_update;

import net.minecraftforge.items.ItemHandlerHelper;

import net.minecraft.world.World;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.Hand;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.projectile.FireballEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Entity;

@Elementsthe_ultimate_combat_update.ModElement.Tag
public class MCreatorFiroRangedItemUsed extends Elementsthe_ultimate_combat_update.ModElement {
	public MCreatorFiroRangedItemUsed(Elementsthe_ultimate_combat_update instance) {
		super(instance, 20);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure MCreatorFiroRangedItemUsed!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure MCreatorFiroRangedItemUsed!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		World world = (World) dependencies.get("world");
		if (!world.isRemote) {
			Entity entityToSpawn = new FireballEntity(EntityType.FIREBALL, world);
			entityToSpawn.setLocationAndAngles(
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 3,
									entity.getLook(1f).y * 3, entity.getLook(1f).z * 3), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getX()),
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 3,
									entity.getLook(1f).y * 3, entity.getLook(1f).z * 3), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getY()),
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 3,
									entity.getLook(1f).y * 3, entity.getLook(1f).z * 3), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getZ()), world.rand.nextFloat() * 360F, 0);
			world.addEntity(entityToSpawn);
		}
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swingArm(Hand.MAIN_HAND);
		}
		if (entity instanceof LivingEntity) {
			((LivingEntity) entity).swingArm(Hand.MAIN_HAND);
		}
		if ((entity.isSneaking())) {
			if (entity instanceof PlayerEntity)
				((PlayerEntity) entity).inventory.clearMatchingItems(p -> new ItemStack(MCreatorFiro.block, (int) (1)).getItem() == p.getItem(),
						(int) 1);
			if (entity instanceof PlayerEntity)
				ItemHandlerHelper.giveItemToPlayer(((PlayerEntity) entity), new ItemStack(MCreatorImpedimenta.block, (int) (1)));
		}
	}
}
